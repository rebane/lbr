#!/usr/bin/python3
# coding: utf-8

import os
import sys
import time
import signal
import getopt
import tempfile
from lxml import etree

tempdir = None
top = True
frame = None
top_place = False

def signal_handler(sig, frame):
	print
	if tempdir != None:
		os.system('rm -rf "' + tempdir + '"')
	sys.exit(255)

signal.signal(signal.SIGINT, signal_handler)

eagle_command = '/opt/eagle-6.1.0/bin/eagle'
eagle_file = None
output_name = None

# opts
opts, args = getopt.getopt(sys.argv[1:], 's:o:btp')

# parse opts & read conversion files
for opt in opts:
	if opt[0] == '-s':
		eagle_file = opt[1]
	elif opt[0] == '-o':
		output_name = opt[1]
	elif opt[0] == '-t':
		top = True
	elif opt[0] == '-b':
		top = False
	elif opt[0] == '-p':
		top_place = True

if len(args) > 0:
	eagle_file = args[0]

input_name = os.path.splitext(eagle_file)[0]
basename = os.path.basename(eagle_file)
if output_name == None:
	if top == True:
		output_name = os.path.splitext(basename)[0] + '_TOP.pdf'
	else:
		output_name = os.path.splitext(basename)[0] + '_BOTTOM.pdf'

# load eagle file
eagle = etree.parse(eagle_file)

# parse frames
eagle_elements = eagle.xpath('/eagle/drawing/board/elements')[0]
for eagle_element in eagle_elements:
	if 'name' in eagle_element.attrib and 'library' in eagle_element.attrib and 'package' in eagle_element.attrib and eagle_element.attrib['library'] == 'frames':
		if eagle_element.attrib['package'] == 'A5L-LOC' or eagle_element.attrib['package'] == 'A4L-LOC' or eagle_element.attrib['package'] == 'A3L-LOC':
			frame = eagle_element.attrib['package']
			break

if frame == None:
	print('missing a frame')
	sys.exit(1)

tempdir = tempfile.mkdtemp(prefix='brd_', dir='/tmp')

if frame == 'A5L-LOC':
	eagle_device = 'PS'
	eagle_scale = 1.3
	eagle_rotation = '-r'
	eagle_offset_x = 0.3
	eagle_offset_y = 0.5
	ps_orientation = '3'
elif frame == 'A4L-LOC':
	eagle_device = 'PS'
	eagle_scale = 1
	eagle_rotation = ''
	eagle_offset_x = 0.5
	eagle_offset_y = 0.2
	ps_orientation = '1'
elif frame == 'A3L-LOC':
	eagle_device = 'PS_DINA3'
	eagle_scale = 1
	eagle_rotation = ''
	eagle_offset_x = 0.5
	eagle_offset_y = 0.5
	ps_orientation = '1'

if top == True:
	if top_place == True:
		brd_command = eagle_command + ' -X -d' + eagle_device + ' -o"' + tempdir + '/brd.ps" -x' + str(eagle_offset_x) + ' -y' + str(eagle_offset_y) + ' ' + eagle_rotation + ' -s' + str(eagle_scale) + ' "' + eagle_file + '" 20 21 48 51 2>&1 | egrep -v "contains a polygon|Continue?|Yes No|EAGLE Version"'
	else:
		brd_command = eagle_command + ' -X -d' + eagle_device + ' -o"' + tempdir + '/brd.ps" -x' + str(eagle_offset_x) + ' -y' + str(eagle_offset_y) + ' ' + eagle_rotation + ' -s' + str(eagle_scale) + ' "' + eagle_file + '" 20 48 51 2>&1 | egrep -v "contains a polygon|Continue?|Yes No|EAGLE Version"'

	ps_command = 'gs -sDEVICE=pdfwrite -sOutputFile="' + output_name + '" -dNOPAUSE -dEPSCrop -dAutoRotatePages=/None -c "<</Orientation ' + ps_orientation + '>> setpagedevice" -f "' + tempdir + '/brd.ps" -c quit'

	# print(brd_command)
	# print(ps_command)
	c = os.popen(brd_command, 'w', 1)
	while True:
		try:
			c.write('Yes\n')
			time.sleep(0.001)
		except:
			break
	# c.close()

	if os.system(ps_command) > 0:
		signal_handler(signal.SIGINT, 0)
else:
	brd_command = eagle_command + ' -X -d' + eagle_device + ' -o"' + tempdir + '/brd_frame.ps" -x' + str(eagle_offset_x) + ' -y' + str(eagle_offset_y) + ' ' + eagle_rotation + ' -s' + str(eagle_scale) + ' "' + eagle_file + '" 48 2>&1 | egrep -v "contains a polygon|Continue?|Yes No|EAGLE Version"'
	ps_command = 'gs -sDEVICE=pdfwrite -sOutputFile="' + tempdir + '/brd_frame.pdf" -dNOPAUSE -dEPSCrop -dAutoRotatePages=/None -c "<</Orientation ' + ps_orientation + '>> setpagedevice" -f "' + tempdir + '/brd_frame.ps" -c quit'

	c = os.popen(brd_command, 'w', 1)
	while True:
		try:
			c.write('Yes\n')
			time.sleep(0.001)
		except:
			break
	# c.close()

	if os.system(ps_command) > 0:
		signal_handler(signal.SIGINT, 0)

	brd_command = eagle_command + ' -X -m -d' + eagle_device + ' -o"' + tempdir + '/brd_layout.ps" -x' + str(eagle_offset_x) + ' -y' + str(eagle_offset_y) + ' ' + eagle_rotation + ' -s' + str(eagle_scale) + ' "' + eagle_file + '" 20 52 2>&1 | egrep -v "contains a polygon|Continue?|Yes No|EAGLE Version"'
	ps_command = 'gs -sDEVICE=pdfwrite -sOutputFile="' + tempdir + '/brd_layout.pdf" -dNOPAUSE -dEPSCrop -dAutoRotatePages=/None -c "<</Orientation ' + ps_orientation + '>> setpagedevice" -f "' + tempdir + '/brd_layout.ps" -c quit'

	c = os.popen(brd_command, 'w', 1)
	while True:
		try:
			c.write('Yes\n')
			time.sleep(0.001)
		except:
			break
	# c.close()

	if os.system(ps_command) > 0:
		signal_handler(signal.SIGINT, 0)

	convert_command = 'pdftk "' + tempdir + '/brd_frame.pdf" background "' + tempdir + '/brd_layout.pdf" output "' + output_name + '"'
	os.system(convert_command)

os.system('rm -rf "' + tempdir + '"')

