#!/usr/bin/python3
# coding: utf-8

import sys
import os
import getopt
import tempfile
xmlstarlet = 'xmlstarlet'

eagle_file = None

# opts
opts, args = getopt.getopt(sys.argv[1:], 's:v:o:')

# parse opts & read conversion files
for opt in opts:
	if opt[0] == '-s':
		eagle_file = opt[1]
	elif opt[0] == '-v':
		version = opt[1]
	elif opt[0] == '-o':
		outfile = opt[1]

if len(args) > 0:
	eagle_file = args[0]

os.system(xmlstarlet + ' fo -D "' + eagle_file + '" | ' + xmlstarlet + ' ed -P -u "eagle/drawing/schematic/parts/part[@library=\'REBANE\' and @deviceset=\'VERSION\']/@value" -v "' + version + '" -u "eagle/drawing/board/elements/element[@library=\'REBANE\' and @package=\'VERSION\']/@value" -v "' + version + '" > "' + outfile + '"')

