#!/usr/bin/python3
# coding: utf-8

import sys
import getopt
from lxml import etree

eagle_file = None
mode = "top"

# opts
opts, args = getopt.getopt(sys.argv[1:], 'tbj')

# parse opts & read conversion files
for opt in opts:
	if opt[0] == '-t':
		mode = "top"
	elif opt[0] == '-b':
		mode = "bottom"
	elif opt[0] == '-j':
		mode = "jlc"

if len(args) > 0:
	eagle_file = args[0]

# load eagle file
eagle = etree.parse(eagle_file)
eagle_elements = eagle.xpath('/eagle/drawing/board/elements')[0]
for eagle_element in eagle_elements:
	if not 'name' in eagle_element.attrib:
		continue
	if not 'x' in eagle_element.attrib:
		continue
	if not 'y' in eagle_element.attrib:
		continue
	if mode == "top":
		if not 'rot' in eagle_element.attrib:
			print(eagle_element.attrib['name'] + ', ' + eagle_element.attrib['x'] + ', ' + eagle_element.attrib['y'] + ', ' + '0')
		else:
			rot = eagle_element.attrib['rot']
			if rot[0] != 'R':
				continue
			rot = rot[1:]
			print(eagle_element.attrib['name'] + ', ' + eagle_element.attrib['x'] + ', ' + eagle_element.attrib['y'] + ', ' + rot)
	elif mode == "bottom":
		if not 'rot' in eagle_element.attrib:
			continue
		else:
			rot = eagle_element.attrib['rot']
			if rot[0] != 'M':
				continue
			if rot[1] != 'R':
				continue
			rot = rot[2:]
			print(eagle_element.attrib['name'] + ', ' + eagle_element.attrib['x'] + ', ' + eagle_element.attrib['y'] + ', ' + rot)
	elif mode == "jlc":
		if not 'rot' in eagle_element.attrib:
			print('\"' + eagle_element.attrib['name'] + '\",\"' + eagle_element.attrib['x'] + '\",\"' + eagle_element.attrib['y'] + '\",\"T\",\"0\"')
		else:
			rot = eagle_element.attrib['rot']
			if rot[0] == 'R':
				rot = rot[1:]
				print('\"' + eagle_element.attrib['name'] + '\",\"' + eagle_element.attrib['x'] + '\",\"' + eagle_element.attrib['y'] + '\",\"T\",\"' + rot + '\"')
			elif rot[0] == 'M' and rot[1] == 'R':
				rot = rot[2:]
				print('\"' + eagle_element.attrib['name'] + '\",\"' + eagle_element.attrib['x'] + '\",\"' + eagle_element.attrib['y'] + '\",\"B\",\"' + rot + '\"')


