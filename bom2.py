#!/usr/bin/python3
# coding: utf-8

# http://eagle.userchat.eng.narkive.com/xNZ1U1vG/automated-release-script#post3

import sys
import io
import codecs
import getopt
import re
import simplejson
import json
import pycurl
import functools
from io import StringIO
from lxml import etree
from odf.opendocument import OpenDocumentSpreadsheet
from odf.style import Style, TableColumnProperties, TableCellProperties, ParagraphProperties, TextProperties
from odf.table import Table, TableRow, TableColumn, TableCell, CoveredTableCell
from odf.text import P

# sys.stdout = codecs.getwriter('utf8')(sys.stdout)
# sys.stderr = codecs.getwriter('utf8')(sys.stderr)

conv = []
parts = []
collected = []
output = []

unmatched = 0
eagle_file = None
fmt = 'txt'
ods_file = None
ods_title = ''

# opts
opts, args = getopt.getopt(sys.argv[1:], 'c:s:o:nf:t:')

# parse opts & read conversion files
for opt in opts:
	if opt[0] == '-c':
		sources = simplejson.loads(open(opt[1], 'r').read())
		if isinstance(sources, list):
			for data in sources:
				source = {}
				source['file'] = opt[1]
				source['data'] = data
				conv.append(source)
		else:
			for name, data in sources.items():
				source = {}
				source['name'] = name
				source['file'] = opt[1]
				source['data'] = data
				conv.append(source)
	elif opt[0] == '-s':
		eagle_file = opt[1]
	elif opt[0] == '-o':
		out = {}
		value = opt[1].split(':')
		if len(value) > 1:
			out['value'] = value[0]
			out['path'] = value[1].split('/')
		else:
			out['path'] = opt[1].split('/')
			out['value'] = out['path'][-1]
		output.append(out)
	elif opt[0] == '-n':
		unmatched = 1
	elif opt[0] == '-f':
		fmt = opt[1]
	elif opt[0] == '-t':
		ods_title = opt[1]

if len(args) > 0:
	ods_file = args[0]

# load eagle file
eagle = etree.parse(eagle_file)
try:
	eagle_parts = eagle.xpath('/eagle/drawing/schematic/parts')[0]
except:
	eagle_parts = eagle.xpath('/eagle/drawing/board/elements')[0]

for eagle_part in eagle_parts:
	part = {}
	part['name'] = eagle_part.attrib['name']
	part['data'] = {}
	part['data']['name'] = eagle_part.attrib['name']
	part['data']['source'] = {}
	part['data']['source']['library'] = eagle_part.attrib['library']
	part['data']['source']['deviceset'] = eagle_part.attrib['deviceset']
	part['data']['source']['device'] = eagle_part.attrib['device']
	if 'value' in eagle_part.attrib:
		part['data']['source']['value'] = eagle_part.attrib['value']
	for child in eagle_part:
		if 'name' in child.attrib and 'value' in child.attrib:
			if child.attrib['name'] == 'VOLTAGE':
				part['data']['source']['voltage'] = child.attrib['value']
			elif child.attrib['name'] == 'TOLERANCE':
				part['data']['source']['tolerance'] = child.attrib['value']
			elif child.attrib['name'] == 'BOMROW':
				part['data']['source']['bomrow'] = child.attrib['value']
	parts.append(part)

# do conversation
for part in parts:
	part_source = part['data']['source']
	for source in conv:
		if (source['data']['action'] == 'change' or source['data']['action'] == 'delete') and len(source['data']['match']) > 0:
			test = 0
			if 'voltage' in part_source:
				if not 'voltage' in source['data']['match']:
					continue
			if 'tolerance' in part_source:
				if not 'tolerance' in source['data']['match']:
					continue
			matched = 1
			for match_name, match_value in source['data']['match'].items():
				if isinstance(match_value, list):
					matched2 = 0
					for match2 in match_value:
						if match_name == 'name':
							if part['name'] == match2:
								matched2 = 1
								break
						else:
							if match_name in part_source:
								if part_source[match_name] == match2:
									matched2 = 1
									break
					if matched2 == 0:
						matched = 0
						break
				else:
					if match_name == 'name':
						if part['name'] != match_value:
							matched = 0
							break
					else:
						if match_name in part_source:
							if match_name == 'voltage':
								if float(part_source[match_name]) > float(match_value):
									matched = 0
									break
							elif match_name == 'tolerance':
								if float(part_source[match_name]) < float(match_value):
									matched = 0
									break
							elif part_source[match_name] != match_value:
								matched = 0
								break
						else:
							if match_name == 'voltage':
								continue
							elif match_name == 'tolerance':
								continue
							matched = 0
							break
			if matched == 1:
				part['data']['action'] = source['data']['action']
				if source['data']['action'] == 'change':
					part['data']['values'] = source['data']['values']
					if 'providers' in source['data']:
						part['data']['providers'] = source['data']['providers']
					else:
						part['data']['providers'] = {}
					if 'unit_quantity' in source['data']:
						part['data']['unit_quantity'] = source['data']['unit_quantity']
					part['data']['mounted'] = 'Yes'
					if 'mounted' in source['data'] and not bool(source['data']['mounted']):
						part['data']['mounted'] = 'No'

# add parts
for source in conv:
	if source['data']['action'] == 'add':
		part = {}
		part['name'] = source['data']['name']
		part['data'] = {}
		part['data']['name'] = source['data']['name']
		part['data']['action'] = source['data']['action']
		part['data']['values'] = source['data']['values']
		if 'providers' in source['data']:
			part['data']['providers'] = source['data']['providers']
		else:
			part['data']['providers'] = {}
		if 'unit_quantity' in source['data']:
			part['data']['unit_quantity'] = source['data']['unit_quantity']
		part['data']['mounted'] = 'Yes'
		if 'mounted' in source['data'] and not bool(source['data']['mounted']):
			part['data']['mounted'] = 'No'
		parts.append(part)

# collect similar parts
for part in parts:
	if 'action' in part['data']:
		if part['data']['action'] == 'change' or part['data']['action'] == 'add':
			matched = 0
			for collected_part in collected:
				if collected_part['data']['values'] != part['data']['values']:
					continue

				if 'bomrow' in part['data']['source']:
					if 'bomrow' in collected_part['data']:
						if collected_part['data']['bomrow'] != part['data']['source']['bomrow']:
							continue
					else:
						continue
				else:
					if 'bomrow' in collected_part['data']:
						continue

				if 'voltage' in part['data']['source']:
					if 'voltage' in collected_part['data']:
						if float(collected_part['data']['voltage']) < float(part['data']['source']['voltage']):
							collected_part['data']['voltage'] = part['data']['source']['voltage']
					else:
						collected_part['data']['voltage'] = part['data']['source']['voltage']
				if 'tolerance' in part['data']['source']:
					if 'tolerance' in collected_part['data']:
						if float(collected_part['data']['tolerance']) > float(part['data']['source']['tolerance']):
							collected_part['data']['tolerance'] = part['data']['source']['tolerance']
					else:
						collected_part['data']['tolerance'] = part['data']['source']['tolerance']

				collected_part['names'].append(part['name'])
				matched = 1
				break

			if matched == 0:
				collected_part = {}
				collected_part['names'] = []
				collected_part['names'].append(part['name'])
				if 'providers' in part['data']:
					collected_part['providers'] = part['data']['providers']
				collected_part['data'] = {}
				if 'source' in part['data'] and 'voltage' in part['data']['source']:
					collected_part['data']['voltage'] = part['data']['source']['voltage']
				if 'source' in part['data'] and 'tolerance' in part['data']['source']:
					collected_part['data']['tolerance'] = part['data']['source']['tolerance']
				if 'source' in part['data'] and 'bomrow' in part['data']['source']:
					collected_part['data']['bomrow'] = part['data']['source']['bomrow']
				collected_part['data']['values'] = part['data']['values']
				collected_part['data']['generated'] = {}
				collected_part['data']['generated']['providers'] = {}
				if 'unit_quantity' in part['data']:
					if float(part['data']['unit_quantity']) == int(part['data']['unit_quantity']):
						collected_part['data']['generated']['unit_quantity'] = int(part['data']['unit_quantity'])
					else:
						collected_part['data']['generated']['unit_quantity'] = float(part['data']['unit_quantity'])
				else:
					collected_part['data']['generated']['unit_quantity'] = 1
				collected_part['data']['mounted'] = part['data']['mounted']
				collected.append(collected_part)

def comp_names(name1, name2):
	name1_number = re.match('.*?([0-9]+)$', name1).group(1)
	name2_number = re.match('.*?([0-9]+)$', name2).group(1)
	return int(name1_number) - int(name2_number)

def concatenate(array):
	count = 0
	name = ''
	for i in array:
		if count == 0:
			name = i
		else:
			name = name + ', ' + i
		count = count + 1
	return name

def concatenate_short(array):
	count = 0
	name = ''
	for i in array:
		if count > 3:
			name = name + ", ..."
			break
		if count == 0:
			name = i
		else:
			name = name + ', ' + i
		count = count + 1
	return name

def jpath(dictionary, path):
	j = dictionary
	found = 1
	if len(path) == 1 and path[0] == "tolerance":
		if path[0] in j:
			return j[path[0]] + "%"
		if "values" in j:
			if "tolerance" in j["values"]:
				return j["values"]["tolerance"]
#	if len(path) == 1 and path[0] == "voltage":
#		if path[0] in j:
#			return j[path[0]] + "V"
#		if "values" in j:
#			if "voltage" in j["values"]:
#				return j["values"]["voltage"]
	for p in path:
		if p in j:
			j = j[p]
		else:
			found = 0
			break
	if found == 1:
		return j
	return None

def provider_data_requested(provider):
	for out in output:
		path = out['path']
		if (len(path) > 3) and (path[0] == 'generated') and (path[1] == 'providers') and (path[2] == provider) and (path[3] != 'codes'):
			return True
	return False

# generated
index = 0
for collected_part in collected:
	collected_part['names'].sort(key=functools.cmp_to_key(comp_names))
	collected_part['data']['generated']['names'] = concatenate(collected_part['names'])
	collected_part['data']['generated']['names_short'] = concatenate_short(collected_part['names'])
	collected_part['data']['generated']['quantity'] = str(collected_part['data']['generated']['unit_quantity'] * len(collected_part['names']))
	collected_part['data']['generated']['index0'] = str(index)
	index = index + 1
	collected_part['data']['generated']['index1'] = str(index)
	val = jpath(collected_part, ['providers'])
	if val != None:
		for provider, data in val.items():
			if 'codes' in data:
				if not provider in collected_part['data']['generated']['providers']:
					collected_part['data']['generated']['providers'][provider] = {}
				collected_part['data']['generated']['providers'][provider]['codes'] = concatenate(data['codes'])
				if provider_data_requested(provider):
					if provider == 'farnell':
						sys.stderr.write('fetching farnell ' + data['codes'][0] + '...')
						try:
							buffer = StringIO()
							c = pycurl.Curl()
							url = "http://api.element14.com//catalog/products?term=id%3A" + data['codes'][0] + "&storeInfo.id=ee.farnell.com&resultsSettings.offset=0&resultsSettings.numberOfResults=1&resultsSettings.refinements.filters=&resultsSettings.responseGroup=medium&callInfo.omitXmlSchema=false&callInfo.callback=&callInfo.responseDataFormat=json&callinfo.apiKey=gd8n8b2kxqw6jq5mutsbrvur";
							c.setopt(c.URL, url)
							c.setopt(c.WRITEDATA, buffer)
							c.perform()
							# c.close()
							farnell = simplejson.loads(buffer.getvalue())["premierFarnellPartNumberReturn"]["products"][0]
							collected_part['data']['generated']['providers'][provider]['name'] = farnell["displayName"]
							inv = 0
							for stock in farnell['stock']['breakdown']:
								if stock['inv'] > 0:
									inv = inv + stock['inv']
							collected_part['data']['generated']['providers'][provider]['stock'] = str(inv)
							mult = 1000000000
							price = 0
							for p in farnell['prices']:
								if p['from'] < mult:
									mult = p['from']
									price = p['cost']
							collected_part['data']['generated']['providers'][provider]['unit_price'] = str(price)
							collected_part['data']['generated']['providers'][provider]['row_price'] = str(price * int(collected_part['data']['generated']['quantity']))
							sys.stderr.write('OK\n')
						except:
							sys.stderr.write('FAILED\n')
						sys.stderr.flush()	

def print_tab(output):
	# find max lengths
	for out in output:
		l = len(out['value'])
		for collected_part in collected:
			val = jpath(collected_part['data'], out['path'])
			if val != None:
				if len(val) > l:
					l = len(val)
		out['length'] = l

	separator = ' | '
	# print names
	count = len(output)
	for out in output:
		count = count - 1
		sys.stdout.write(out['value'])
		if count > 0:
			sys.stdout.write((' ' * (out['length'] - len(out['value']))))
			sys.stdout.write(separator)
	sys.stdout.write('\n')
	sys.stdout.flush()

	# print horizontal separator
	count = len(output)
	for out in output:
		count = count - 1
		sys.stdout.write(('-' * (out['length'])))
		if count > 0:
			sys.stdout.write(separator)
	sys.stdout.write('\n')
	sys.stdout.flush()

	# print parts
	for collected_part in collected:
		count = len(output)
		for out in output:
			count = count - 1
			val = jpath(collected_part['data'], out['path'])
			if val != None:
				sys.stdout.write(val)
				if count > 0:
					sys.stdout.write((' ' * (out['length'] - len(val))))
					sys.stdout.write(separator)
			else:
				if count > 0:
					sys.stdout.write((' ' * (out['length'])))
					sys.stdout.write(separator)
		sys.stdout.write('\n')
		sys.stdout.flush()

def print_csv(output):
	# print names
	count = len(output)
	for out in output:
		count = count - 1
		sys.stdout.write('\"')
		sys.stdout.write(out['value'])
		sys.stdout.write('\"')
		if count > 0:
			sys.stdout.write(',')
	sys.stdout.write('\n')
	sys.stdout.flush()

	# print parts
	for collected_part in collected:
		count = len(output)
		for out in output:
			count = count - 1
			val = jpath(collected_part['data'], out['path'])
			sys.stdout.write('\"')
			if val != None:
				sys.stdout.write(val)
			sys.stdout.write('\"')
			if count > 0:
				sys.stdout.write(',')
		sys.stdout.write('\n')
		sys.stdout.flush()

def print_ods(output):
	ods = OpenDocumentSpreadsheet()
	table = Table(name = ods_title)

	multiplier = 201
	add = 500
	col = 0

	# find max lengths
	for out in output:
		l = len(out['value'])
		for collected_part in collected:
			val = jpath(collected_part['data'], out['path'])
			if val != None:
				if len(val) > l:
					l = len(val)
		colstyle = Style(name = 'col' + str(col), family = 'table-column')
		colstyle.addElement(TableColumnProperties(columnwidth = (multiplier * l) + add))
		ods.automaticstyles.addElement(colstyle)
		table.addElement(TableColumn(stylename = 'col' + str(col)))
		col = col + 1

	titlestyle = Style(name = 'titlestyle', family = 'table-cell')
	titlestyle.addElement(TextProperties(fontfamily = 'Bitstream Vera Sans Mono', fontsize = 10))
	titlestyle.addElement(TableCellProperties(backgroundcolor = '#0000ff'))
	titlestyle.addElement(ParagraphProperties(textalign = 'center'))
	ods.styles.addElement(titlestyle)

	namestyle = Style(name = 'namestyle', family = 'table-cell')
	namestyle.addElement(TextProperties(fontfamily = 'Bitstream Vera Sans Mono', fontsize = 10))
	namestyle.addElement(TableCellProperties(backgroundcolor = '#00ff00'))
	ods.styles.addElement(namestyle)

	namestyle = Style(name = 'notmountedstyle', family = 'table-cell')
	namestyle.addElement(TextProperties(fontfamily = 'Bitstream Vera Sans Mono', fontsize = 10))
	namestyle.addElement(TableCellProperties(backgroundcolor = '#ff0000'))
	ods.styles.addElement(namestyle)

	partstyle = Style(name = 'partstyle', family = 'table-cell')
	partstyle.addElement(TextProperties(fontfamily = 'Bitstream Vera Sans Mono', fontsize = 10))
	ods.styles.addElement(partstyle)

	ods.spreadsheet.addElement(table)

	# print title
	tr = TableRow()
	table.addElement(tr)
	tc = TableCell(numbercolumnsspanned = len(output), stylename = 'titlestyle')
	tc.addElement(P(text = ods_title))
	tr.addElement(tc)

	# print names
	tr = TableRow()
	table.addElement(tr)
	for out in output:
		tc = TableCell(stylename = 'namestyle')
		val = jpath(collected_part['data'], out['path'])
		tc.addElement(P(text = out['value']))
		tr.addElement(tc)

	# print parts
	for collected_part in collected:
		tr = TableRow()
		table.addElement(tr)
		for out in output:
			if collected_part['data']['mounted'] == 'No':
				tc = TableCell(stylename = 'notmountedstyle')
			else:
				tc = TableCell(stylename = 'partstyle')
			val = jpath(collected_part['data'], out['path'])
			if val != None:
				tc.addElement(P(text = val))
			else:
				tc.addElement(P(text = ''))
			tr.addElement(tc)
	ods.save(ods_file)

if fmt == 'txt':
	print_tab(output)
elif fmt == 'csv':
	print_csv(output)
elif fmt == 'ods':
	print_ods(output)
else:
	sys.stderr.write('UNKNOWN FORMAT\n')
	sys.exit(-1)

# print unmatched parts
if unmatched == 1:
	count = 0
	for part in parts:
		if not 'action' in part['data']:
			count = count + 1
			sys.stderr.write('UNMATCHED: ' + part['data']['name'] + ', ' + simplejson.dumps(part['data']['source'], encoding='utf-8') + '\n')
			sys.stderr.flush()
	if count > 0:
		sys.exit(-1)

sys.exit(0)
