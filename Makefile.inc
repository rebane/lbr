EAGLE ?= /opt/eagle-6.1.0/bin/eagle
XMLSTARLET ?= xmlstarlet
TOP_SILK ?= "21,25"
BOTTOM_SILK ?= "22,26"

.PHONY: all bomtxt bomcsv version bom sch mnt mnt_top mnt_bottom layout_top layout_bottom gerbers rel

all:
	rm -rf "release/test"
	if [ ! -d release ]; then mkdir release; fi
	mkdir "release/test"
	cp "$(BASENAME).json" "release/test/$(BASENAME)$(VERSION).json"
	make version
	make bom
	make sch
	make mnt
	make mnt_top
	make mnt_bottom
	make layout_top
	make layout_bottom
	make gerbers

bomtxt:
	$(BOMLIB_PATH)/bom.py $(BOM_ARGS) $(BOM_NAMES) -c "$(BASENAME).json" -S -s "$(BASENAME).sch" -n -f txt

bomcsv:
	$(BOMLIB_PATH)/bom.py -c $(BOM_ARGS) -c "$(BASENAME).json" -o "Comment:values/value" -o "Designator:generated/names" -o "Footprint:values/package" -o "LCSC:generated/providers/jlcpcb/codes" -s "$(BASENAME).sch" -n -f csv

version:
	$(BOMLIB_PATH)/version.py -s "$(BASENAME).sch" -v "$(VERSION)" -o "release/test/$(BASENAME)$(VERSION).sch"
	$(BOMLIB_PATH)/version.py -s "$(BASENAME).brd" -v "$(VERSION)" -o "release/test/$(BASENAME)$(VERSION).brd"

bom:
	$(BOMLIB_PATH)/bom.py $(BOM_ARGS) -c "release/test/$(BASENAME)$(VERSION).json" $(BOM_NAMES) -S -s "release/test/$(BASENAME)$(VERSION).sch" -n -f txt > "release/test/$(BASENAME)$(VERSION)_bom.txt"
	$(BOMLIB_PATH)/bom.py $(BOM_ARGS) -c "release/test/$(BASENAME)$(VERSION).json" -o "Comment:values/value" -o "Designator:generated/names" -o "Footprint:values/package" -o "LCSC:generated/providers/jlcpcb/codes" -s "release/test/$(BASENAME)$(VERSION).sch" -n -f csv >"release/test/$(BASENAME)$(VERSION)_bom.csv"
	$(BOMLIB_PATH)/bom.py $(BOM_ARGS) -c "release/test/$(BASENAME)$(VERSION).json" -s "release/test/$(BASENAME)$(VERSION).sch" -n -f ods -t "$(BASENAME) ($(VERSION))" "release/test/$(BASENAME)$(VERSION)_bom.ods"
	cd "release/test" && soffice --headless --convert-to xlsx "$(BASENAME)$(VERSION)_bom.ods"

sch:
	$(BOMLIB_PATH)/schematic.py -s "release/test/$(BASENAME)$(VERSION).sch" -o "release/test/$(BASENAME)$(VERSION)_sch.pdf"

mnt:
	/bin/echo -e "\"Designator\",\"Mid X\",\"Mid Y\",\"Layer\",\"Rotation\"" >"release/test/$(BASENAME)$(VERSION)_pos.csv"
	$(BOMLIB_PATH)/mnt.py -j "release/test/$(BASENAME)$(VERSION).brd" >> "release/test/$(BASENAME)$(VERSION)_pos.csv"

mnt_top:
	/bin/echo -e "$(BASENAME)$(VERSION)\n" >"release/test/$(BASENAME)$(VERSION)_top.mnt"
	$(BOMLIB_PATH)/mnt.py "release/test/$(BASENAME)$(VERSION).brd" >> "release/test/$(BASENAME)$(VERSION)_top.mnt"

mnt_bottom:
	/bin/echo -e "$(BASENAME)$(VERSION)\n" >"release/test/$(BASENAME)$(VERSION)_bottom.mnt"
	$(BOMLIB_PATH)/mnt.py -b "release/test/$(BASENAME)$(VERSION).brd" >> "release/test/$(BASENAME)$(VERSION)_bottom.mnt"

layout_top:
	$(BOMLIB_PATH)/layout.py -t -s "release/test/$(BASENAME)$(VERSION).brd" -o "release/test/$(BASENAME)$(VERSION)_top.pdf"

layout_bottom:
	$(BOMLIB_PATH)/layout.py -b -s "release/test/$(BASENAME)$(VERSION).brd" -o "release/test/$(BASENAME)$(VERSION)_bottom.pdf"

gerbers:
	$(BOMLIB_PATH)/gerber.py -S white -M blue -t $(TOP_SILK) -b $(BOTTOM_SILK) -p "release/test/gerbers" "release/test/$(BASENAME)$(VERSION).brd"
	cd "release/test/gerbers" && mv "$(BASENAME)$(VERSION).INFO" .. && zip -r "$(BASENAME)$(VERSION).zip" * && mv "../$(BASENAME)$(VERSION).INFO" .

rel:
	if [ -d "release/$(BASENAME)$(VERSION)" ]; then /bin/echo -e "\nRelease directory exists!!!\n" >&2; return 1; fi
	mkdir "release/$(BASENAME)$(VERSION)"
	cp -r release/test/* "release/$(BASENAME)$(VERSION)/"

