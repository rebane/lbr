#!/usr/bin/python3
# coding: utf-8

# with tPlace: (tPlace, tNames, tTest)
#   -t "21,25,37"
# with bPlace: (bPlace, bNames, bTest)
#   -b "22,26,38"

import os
import sys
import time
import signal
import getopt
import json
import re
from lxml import etree

def signal_handler(sig, frame):
	print
	sys.exit(255)

signal.signal(signal.SIGINT, signal_handler)

layers = {}

eagle_command = '/opt/eagle-6.1.0/bin/eagle'
eagle_file = None
output_path = 'gerber'
output_name = None
gerbers = {}
pcb_info = {
	'material': {
		'type': 'FR4',
		'glass_transition_temperature': {
			'unit': 'centigrade',
			'min': 150,
			'max': 160
		},
	},
	'num_of_layers': 2,
	'dimensions': {
		'unit': 'mm',
		'values': [0, 0]
	},
	'thickness': {
		'unit': 'mm',
		'value': 1.6
	},
	'milling': False,
	'final_cu_thickness': {
		'unit': 'oz',
		'top layer': 1,
		'bottom layer': 1
	},
	'min_track': {
		'unit': 'mil',
		'value': 6
	},
	'min_spacing': {
		'unit': 'mil',
		'value': 6
	},
	'min_hole': {
		'unit': 'mm',
		'value': 0.3
	},
	'mask_color': 'green',
	'silk_color': 'white',
	'gold_fingers': False,
	'surface_finish': 'HASL with lead',
	'via_process': 'tenting vias',
	'file_ext': {
		'INFO': 'json information'
	},
	'order_from_top_to_bottom': [
	]
}

def gerbers_add(name, fmt, ext, used, layers):
	gerber_data = {}
	gerber_data['name'] = name
	gerber_data['format'] = fmt
	gerber_data['extension'] = ext
	gerber_data['used'] = used
	gerber_data['layers'] = layers
	gerbers[name] = gerber_data

gerbers_add('drill',                'EXCELLON',      'TXT',   True,  [20, 44, 45])
gerbers_add('outline',              'GERBER_RS274X', 'GML',   True,  [20, 46])
gerbers_add('bottom mask',          'GERBER_RS274X', 'GBS',   True,  [30])
gerbers_add('top mask',             'GERBER_RS274X', 'GTS',   True,  [29])
gerbers_add('bottom silk',          'GERBER_RS274X', 'GBO',   True,  [22, 26])
gerbers_add('top silk',             'GERBER_RS274X', 'GTO',   True,  [21, 25])
gerbers_add('bottom layer',         'GERBER_RS274X', 'GBL',   True,  [16, 17, 18])
gerbers_add('layer2',               'GERBER_RS274X', 'GL2',   False, [2, 17, 18])
gerbers_add('layer3',               'GERBER_RS274X', 'GL3',   False, [15, 17, 18])
gerbers_add('top layer',            'GERBER_RS274X', 'GTL',   True,  [1, 17, 18])
gerbers_add('top paste/stencil',    'GERBER_RS274X', 'GTP',   True,  [31])
gerbers_add('bottom paste/stencil', 'GERBER_RS274X', 'GBP',   True,  [32])
gerbers_add('v-cut',                'GERBER_RS274X', 'VCUT',  False, [129])
gerbers_add('top coating',          'GERBER_RS274X', 'TCOAT', False, [33])
gerbers_add('bottom coating',       'GERBER_RS274X', 'BCOAT', False, [34])

# opts
opts, args = getopt.getopt(sys.argv[1:], 's:p:n:t:b:S:M:T:')

# parse opts & read conversion files
for opt in opts:
	if opt[0] == '-s':
		eagle_file = opt[1]
	elif opt[0] == '-p':
		output_path = opt[1]
	elif opt[0] == '-n':
		output_name = opt[1]
	elif opt[0] == '-t':
		# gerber_top_silk = opt[1]
		gerbers['top silk']['layers'] = opt[1].split(',')
	elif opt[0] == '-b':
		# gerber_bottom_silk = opt[1]
		gerbers['bottom silk']['layers'] = opt[1].split(',')
	elif opt[0] == '-S':
		pcb_info['silk_color'] = opt[1]
	elif opt[0] == '-M':
		pcb_info['mask_color'] = opt[1]
	elif opt[0] == '-T':
		pcb_info['thickness']['value'] = float(opt[1])

if len(args) > 0:
	eagle_file = args[0]

if output_name == None:
	basename = os.path.basename(eagle_file)
	output_name = os.path.splitext(basename)[0]

# load eagle file
eagle = etree.parse(eagle_file)

# read layers
eagle_layers = eagle.xpath('/eagle/drawing/layers')[0]
for eagle_layer in eagle_layers:
	layer_name = eagle_layer.attrib['name']
	layer_data = {}
	layer_data['name'] = layer_name
	layer_data['number'] = int(eagle_layer.attrib['number'])
	layer_data['used'] = False
	layers[layer_name] = layer_data

def layer_by_number(layer_number):
	for layer_name in layers:
		layer_data = layers[layer_name]
		if layer_data['number'] == int(layer_number):
			return layer_data
	return {}

def layer_find_used(et):
	for item in et:
		if 'layer' in item.attrib:
			layer = layer_by_number(item.attrib['layer'])
			if 'number' in layer:
				layer['used'] = True
		layer_find_used(item)

# detect used layers
layer_find_used(eagle.xpath('/eagle/drawing/board')[0])

# detect num of layers
num_of_layers = 2
layer = layer_by_number(2)
if 'used' in layer and layer['used']:
	num_of_layers = 4

layer = layer_by_number(15)
if 'used' in layer and layer['used']:
	num_of_layers = 4

pcb_info['num_of_layers'] = num_of_layers
if num_of_layers >= 4:
	gerbers['layer2']['used'] = True
	gerbers['layer3']['used'] = True
	pcb_info['final_cu_thickness']['layer2'] = 1
	pcb_info['final_cu_thickness']['layer3'] = 1
	pcb_info['order_from_top_to_bottom'] = ['GTL', 'GL2', 'GL3', 'GBL']
else:
	pcb_info['order_from_top_to_bottom'] = ['GTL', 'GBL']

# detect V-CUT
layer = layer_by_number(129)
if 'used' in layer and layer['used']:
	gerbers['v-cut']['used'] = True

# detect top coating
layer = layer_by_number(33)
if 'used' in layer and layer['used']:
	gerbers['top coating']['used'] = True

# detect bottom coating
layer = layer_by_number(34)
if 'used' in layer and layer['used']:
	gerbers['bottom coating']['used'] = True

# detect panelized name layers
panelized_names = False
layer = layer_by_number(125)
if 'used' in layer and layer['used']:
	panelized_names = True
layer = layer_by_number(126)
if 'used' in layer and layer['used']:
	panelized_names = True

if panelized_names:
	gerber_layers = []
	for layer in gerbers['top silk']['layers']:
		if layer == 25:
			gerber_layers.append(125)
		else:
			gerber_layers.append(layer)
	gerbers['top silk']['layers'] = gerber_layers
	gerber_layers = []
	for layer in gerbers['bottom silk']['layers']:
		if layer == 26:
			gerber_layers.append(126)
		else:
			gerber_layers.append(layer)
	gerbers['bottom silk']['layers'] = gerber_layers

# make output dir if needed
if not os.path.exists(output_path):
	os.mkdir(output_path)

# generate gerbers
for gerber_name in gerbers:
	gerber_data = gerbers[gerber_name]
	if gerber_data['used']:
		command = eagle_command + ' -X -d' + gerber_data['format'] + ' -o"' + output_path + '/' + output_name + '.' + gerber_data['extension'] + '" "' + eagle_file + '"'
		for layer in gerber_data['layers']:
			command = command + ' ' + str(layer)
		command = command + ' 2>&1 | egrep -v "contains a polygon|Continue?|Yes No|EAGLE Version"'

		layer_names = ''
		for layer in gerber_data['layers']:
			layer = layer_by_number(layer)
			if len(layer_names):
				layer_names = layer_names + ', '
			layer_names = layer_names + '"' + layer['name'] + '"'
		job_printout = 'Generating "' + gerber_data['name'] + '" from layer(s) ' + layer_names + ':\033[0m'
		if gerber_data['name'] == 'top silk' or gerber_data['name'] == 'bottom silk':
			job_printout = '\033[1;37m' + job_printout
		elif gerber_data['name'] == 'top mask' or gerber_data['name'] == 'bottom mask':
			job_printout = '\033[1;32m' + job_printout
		elif gerber_data['name'] == 'top layer' or gerber_data['name'] == 'bottom layer' or gerber_data['name'] == 'layer2' or gerber_data['name'] == 'layer3':
			job_printout = '\033[1;31m' + job_printout
		elif gerber_data['name'] == 'drill':
			job_printout = '\033[1;33m' + job_printout
		elif gerber_data['name'] == 'top stencil':
			job_printout = '\033[1;34m' + job_printout
		elif gerber_data['name'] == 'outline':
			job_printout = '\033[1;36m' + job_printout
		elif gerber_data['name'] == 'v-cut':
			job_printout = '\033[1;35m' + job_printout
		print
		print(job_printout)

		c = os.popen(command, 'w', 1)
		while True:
			try:
				c.write('Yes\n')
				time.sleep(0.001)
			except:
				break
		# c.close()
		pcb_info['file_ext'][gerber_data['extension']] = gerber_data['name']

# remove dri file
dri_file = output_path + '/' + output_name + '.dri'
if os.path.exists(dri_file):
	os.remove(dri_file)

# remove gpi file
gpi_file = output_path + '/' + output_name + '.gpi'
if os.path.exists(gpi_file):
	os.remove(gpi_file)

# calculate dimension
outline_file = output_path + '/' + output_name + '.' + gerbers['outline']['extension']
xmin = None
xmax = None
ymin = None
ymax = None
with open(outline_file, 'r') as f:
	line = f.readline()
	while line:
		results = re.search("^X([\d-]+)Y([\d-]+)", line.strip())
		if results:
			x = int(results.group(1))
			y = int(results.group(2))
			xmin = min(xmin, x) if xmin else x
			xmax = max(xmax, x) if xmax else x
			ymin = min(ymin, y) if ymin else y
			ymax = max(ymax, y) if ymax else y
		line = f.readline()
	f.close()

w = xmax - xmin
h = ymax - ymin
w_in = w / 10000.0
h_in = h / 10000.0
w_mm = round(w_in * 25.4, 1)
h_mm = round(h_in * 25.4, 1)
pcb_info['dimensions']['values'] = [w_mm, h_mm]

# generate info
print
print('\033[0;33mGenerating info:\033[0m')

info_file = output_path + '/' + output_name + '.INFO'
with open(info_file, 'w') as f:
	json.dump(pcb_info, f, sort_keys=True, indent=4)
	f.write('\n')

print('PCB dimensions (mm): ' + str(w_mm) + ' x ' + str(h_mm))
print('finished.')
print

