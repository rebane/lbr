#ifndef _ATTR_H_
#define _ATTR_H_

#define ATTR_FIRST_SQUARE    0x01
#define ATTR_FLIP_ROWS       0x02
#define ATTR_FLIP_PIN_NAMES  0x04
#define ATTR_BOTTOM_PLACE    0x08
#define ATTR_BOTTOM_DOC      0x10
#define ATTR_BOTTOM_NAME     0x20
#define ATTR_BOTTOM_NAME_DOC 0x40

#endif

