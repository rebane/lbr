#include "symbol.h"
#include <stdio.h>
#include "attr.h"

static void symbol1(char *name, int start, int end, int rows, uint32_t attrs);
static void symbol2(char *name, int start, int end, int rows, uint32_t attrs);

void symbol(char *name, int start, int end, int rows, uint32_t attrs)
{
	puts("\t\t\t<symbols>");

	if (rows == 1)
		symbol1(name, start, end, rows, attrs);
	else if (rows == 2)
		symbol2(name, start, end, rows, attrs);

	puts("\t\t\t</symbols>");
}

static void symbol1(char *name, int start, int end, int rows, uint32_t attrs)
{
	float y_loc;
	int i, column;

	for (i = start; i <= end; i++) {
		printf("\t\t\t\t<symbol name=\"%s-%02d\">\n", name, i);

		y_loc = (2.54 * (i / 2));

		for (column = 0; column < i; column++) {
			printf("\t\t\t\t\t<pin name=\"%d\" x=\"2.54\" y=\"%.02f\" visible=\"off\" length=\"short\" direction=\"pas\" swaplevel=\"1\" rot=\"R180\"/>\n", (column + 1), y_loc);
			y_loc -= 2.54;
		}
	
		y_loc = (2.54 * (i / 2));
		y_loc -= 0.716;

		for (column = 0; column < i; column++) {
			if (column < 9)
				printf("\t\t\t\t\t<text x=\"-1.778\" y=\"%.02f\" size=\"1.4224\" layer=\"95\" font=\"vector\">%d</text>\n", y_loc, (column + 1));
			else
				printf("\t\t\t\t\t<text x=\"-2.413\" y=\"%.02f\" size=\"1.4224\" layer=\"95\" font=\"vector\">%d</text>\n", y_loc, (column + 1));

			y_loc -= 2.54;
		}

		y_loc = (2.54 * (i / 2)) + 2.54;

		printf("\t\t\t\t\t<wire x1=\"-2.54\" y1=\"%.02f\" x2=\"0\" y2=\"%.02f\" width=\"0.254\" layer=\"94\"/>\n", y_loc, y_loc);
		printf("\t\t\t\t\t<wire x1=\"0\" y1=\"%.02f\" x2=\"0\" y2=\"%.02f\" width=\"0.254\" layer=\"94\"/>\n", y_loc, y_loc - (2.54 * (i + 1)));
		printf("\t\t\t\t\t<wire x1=\"0\" y1=\"%.02f\" x2=\"-2.54\" y2=\"%.02f\" width=\"0.254\" layer=\"94\"/>\n", y_loc - (2.54 * (i + 1)), y_loc - (2.54 * (i + 1)));
		printf("\t\t\t\t\t<wire x1=\"-2.54\" y1=\"%.02f\" x2=\"-2.54\" y2=\"%.02f\" width=\"0.254\" layer=\"94\"/>\n", y_loc - (2.54 * (i + 1)), y_loc);

		printf("\t\t\t\t\t<text x=\"-2.54\" y=\"%.02f\" size=\"1.778\" layer=\"95\" font=\"vector\">&gt;Name</text>\n", y_loc + 2.54);
		printf("\t\t\t\t\t<text x=\"0\" y=\"%.02f\" size=\"1.778\" layer=\"96\" font=\"vector\" rot=\"R180\">&gt;Value</text>\n", y_loc - (2.54 * (i + 2)));

		puts("\t\t\t\t</symbol>");
	}
}

static void symbol2(char *name, int start, int end, int rows, uint32_t attrs)
{
	float y_loc;
	int i, column, pin;

	for (i = start; i <= end; i++) {
		printf("\t\t\t\t<symbol name=\"%s-%02d\">\n", name, i);

		y_loc = (2.54 * (i / 2));
		for (column = 0; column < i; column++) {
			if (attrs & ATTR_FLIP_PIN_NAMES)
				pin = column + 1;
			else
				pin = (column * 2) + 1;

			printf("\t\t\t\t\t<pin name=\"%d\" x=\"5.08\" y=\"%.02f\" visible=\"off\" length=\"short\" direction=\"pas\" swaplevel=\"1\" rot=\"R180\"/>\n", pin, y_loc);
			y_loc -= 2.54;
		}

		y_loc = (2.54 * (i / 2));
		for (column = 0; column < i; column++) {
			if (attrs & ATTR_FLIP_PIN_NAMES)
				pin = i + column + 1;
			else
				pin = (column * 2) + 2;

			printf("\t\t\t\t\t<pin name=\"%d\" x=\"-5.08\" y=\"%.02f\" visible=\"off\" length=\"short\" direction=\"pas\" swaplevel=\"1\"/>\n", pin, y_loc);
			y_loc -= 2.54;
		}

		y_loc = (2.54 * (i / 2));
		y_loc -= 0.716;
		for (column = 0; column < i; column++) {
			if (attrs & ATTR_FLIP_PIN_NAMES)
				pin = column + 1;
			else
				pin = (column * 2) + 1;

			if (pin <= 9)
				printf("\t\t\t\t\t<text x=\"0.762\" y=\"%.02f\" size=\"1.4224\" layer=\"95\" font=\"vector\">%d</text>\n", y_loc, pin);
			else
				printf("\t\t\t\t\t<text x=\"0.127\" y=\"%.02f\" size=\"1.4224\" layer=\"95\" font=\"vector\">%d</text>\n", y_loc, pin);

			y_loc -= 2.54;
		}

		y_loc = (2.54 * (i / 2));
		y_loc -= 0.716;
		for (column = 0; column < i; column++) {
			if (attrs & ATTR_FLIP_PIN_NAMES)
				pin = i + column + 1;
			else
				pin = (column * 2) + 2;

			if (pin <= 9)
				printf("\t\t\t\t\t<text x=\"-1.778\" y=\"%.02f\" size=\"1.4224\" layer=\"95\" font=\"vector\">%d</text>\n", y_loc, pin);
			else
				printf("\t\t\t\t\t<text x=\"-2.413\" y=\"%.02f\" size=\"1.4224\" layer=\"95\" font=\"vector\">%d</text>\n", y_loc, pin);

			y_loc -= 2.54;
		}

		y_loc = (2.54 * (i / 2)) + 2.54;
		printf("\t\t\t\t\t<wire x1=\"-2.54\" y1=\"%.02f\" x2=\"2.54\" y2=\"%.02f\" width=\"0.254\" layer=\"94\"/>\n", y_loc, y_loc);
		printf("\t\t\t\t\t<wire x1=\"2.54\" y1=\"%.02f\" x2=\"2.54\" y2=\"%.02f\" width=\"0.254\" layer=\"94\"/>\n", y_loc, y_loc - (2.54 * (i + 1)));
		printf("\t\t\t\t\t<wire x1=\"2.54\" y1=\"%.02f\" x2=\"-2.54\" y2=\"%.02f\" width=\"0.254\" layer=\"94\"/>\n", y_loc - (2.54 * (i + 1)), y_loc - (2.54 * (i + 1)));
		printf("\t\t\t\t\t<wire x1=\"-2.54\" y1=\"%.02f\" x2=\"-2.54\" y2=\"%.02f\" width=\"0.254\" layer=\"94\"/>\n", y_loc - (2.54 * (i + 1)), y_loc);

		printf("\t\t\t\t\t<text x=\"-2.54\" y=\"%.02f\" size=\"1.778\" layer=\"95\" font=\"vector\">&gt;Name</text>\n", y_loc + 2.54);
		printf("\t\t\t\t\t<text x=\"2.54\" y=\"%.02f\" size=\"1.778\" layer=\"96\" font=\"vector\" rot=\"R180\">&gt;Value</text>\n", y_loc - (2.54 * (i + 2)));

		puts("\t\t\t\t</symbol>");
	}
}

