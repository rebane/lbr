#include "device.h"
#include <stdio.h>

void device(char *name, int start, int end, int rows, char *prefix, char *technology)
{
	int i, j;

	puts("\t\t\t<devicesets>");

	for (i = start; i <= end; i++) {
		printf("\t\t\t\t<deviceset name=\"%s-%02d\" prefix=\"%s\" uservalue=\"yes\">\n", name, i, prefix);
		printf("\t\t\t\t\t<description>\"%s-%02d\"</description>\n", name, i);
		puts("\t\t\t\t\t<gates>");
		printf("\t\t\t\t\t\t<gate name=\"%s\" symbol=\"%s-%02d\" x=\"0\" y=\"0\"/>\n", prefix, name, i);
		puts("\t\t\t\t\t</gates>");
		puts("\t\t\t\t\t<devices>");
		printf("\t\t\t\t\t\t<device name=\"%s\" package=\"%s-%02d\">\n", technology, name, i);
		puts("\t\t\t\t\t\t\t<connects>");
		for (j = 0; j < (i * rows); j++)
			printf("\t\t\t\t\t\t\t\t<connect gate=\"%s\" pin=\"%d\" pad=\"%d\"/>\n", prefix, (j + 1), (j + 1));

		puts("\t\t\t\t\t\t\t</connects>");
		puts("\t\t\t\t\t\t\t<technologies>");
		printf("\t\t\t\t\t\t\t\t<technology name=\"%s\"/>\n", technology);
		puts("\t\t\t\t\t\t\t</technologies>");
		puts("\t\t\t\t\t\t</device>");
		puts("\t\t\t\t\t</devices>");
		puts("\t\t\t\t</deviceset>");
	}

	puts("\t\t\t</devicesets>");
}

