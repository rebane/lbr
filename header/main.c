#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include "attr.h"
#include "package.h"
#include "symbol.h"
#include "device.h"

void header();
void footer();

int main(int argc, char *argv[])
{
	uint32_t attrs = 0; // ATTR_BOTTOM_PLACE | ATTR_BOTTOM_DOC | ATTR_BOTTOM_NAME | ATTR_BOTTOM_NAME_DOC; // for female: ATTR_FLIP_ROWS
	char *name = "TEST";
	int start = 1;
	int end = 50;
	int rows = 1;
	float pitch = 2.54;
	float drill = 1.0;
	char buffer[64];
	int c;

	snprintf(buffer, 64, "-2.54");

	while ((c = getopt(argc, argv, "hfsn:r:p:d:")) != -1) {
		if (c == 'h') {
			fprintf(stderr, "HELP\n");
			return 1;
		} else if (c == 'f') {
			attrs |= ATTR_FLIP_ROWS;
		} else if (c == 's') {
			attrs |= ATTR_FIRST_SQUARE;
		} else if (c == 'n') {
			name = optarg;
		} else if (c == 'r') {
			rows = atoi(optarg);;
		} else if (c == 'p') {
			pitch = strtof(optarg, NULL);
			snprintf(buffer, 64, "-%s", optarg);
		} else if (c == 'd') {
			drill = strtof(optarg, NULL);
		}
	}

	buffer[63] = 0;

	header();
	package(name, start, end, rows, pitch, pitch, drill, 0, (pitch / 2), (pitch / 2), (pitch / 2), (pitch / 2), attrs);
	symbol(name, start, end, rows, attrs);
	device(name, start, end, rows, "JP", buffer);
	footer();

	return 0;
}

void header()
{
	puts("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
	puts("<!DOCTYPE eagle SYSTEM \"eagle.dtd\">");
	puts("<eagle version=\"6.1\">");
	puts("\t<drawing>");
	puts("\t\t<settings>");
	puts("\t\t\t<setting alwaysvectorfont=\"yes\"/>");
	puts("\t\t\t<setting verticaltext=\"up\"/>");
	puts("\t\t</settings>");
	puts("\t\t<grid distance=\"0.1\" unitdist=\"mm\" unit=\"mm\" style=\"lines\" multiple=\"1\" display=\"no\" altdistance=\"0.01\" altunitdist=\"mm\" altunit=\"mm\"/>");
	puts("\t\t<layers>");
	puts("\t\t\t<layer number=\"1\" name=\"Top\" color=\"4\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"16\" name=\"Bottom\" color=\"1\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"17\" name=\"Pads\" color=\"2\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"18\" name=\"Vias\" color=\"2\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"20\" name=\"Dimension\" color=\"15\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"21\" name=\"tPlace\" color=\"7\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"22\" name=\"bPlace\" color=\"7\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"23\" name=\"tOrigins\" color=\"15\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"24\" name=\"bOrigins\" color=\"15\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"25\" name=\"tNames\" color=\"7\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"26\" name=\"bNames\" color=\"7\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"27\" name=\"tValues\" color=\"7\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"28\" name=\"bValues\" color=\"7\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"29\" name=\"tStop\" color=\"7\" fill=\"3\" visible=\"no\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"30\" name=\"bStop\" color=\"7\" fill=\"6\" visible=\"no\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"31\" name=\"tCream\" color=\"7\" fill=\"4\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"32\" name=\"bCream\" color=\"7\" fill=\"5\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"41\" name=\"tRestrict\" color=\"4\" fill=\"10\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"42\" name=\"bRestrict\" color=\"1\" fill=\"10\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"44\" name=\"Drills\" color=\"7\" fill=\"1\" visible=\"no\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"45\" name=\"Holes\" color=\"7\" fill=\"1\" visible=\"no\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"46\" name=\"Milling\" color=\"3\" fill=\"1\" visible=\"no\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"51\" name=\"tDocu\" color=\"14\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"52\" name=\"bDocu\" color=\"13\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"90\" name=\"Modules\" color=\"5\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"91\" name=\"Nets\" color=\"2\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"92\" name=\"Busses\" color=\"1\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"93\" name=\"Pins\" color=\"2\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"94\" name=\"Symbols\" color=\"4\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"95\" name=\"Names\" color=\"7\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t\t<layer number=\"96\" name=\"Values\" color=\"7\" fill=\"1\" visible=\"yes\" active=\"yes\"/>");
	puts("\t\t</layers>");
	puts("\t\t<library>");
}

void footer()
{
	puts("\t\t</library>");
	puts("\t</drawing>");
	puts("</eagle>");
	puts("");
}

