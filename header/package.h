#ifndef _PACKAGE_H_
#define _PACKAGE_H_

#include <stdint.h>

void package(char *name, int start, int end, int rows, float pitch, float row_pitch, float drill, float diameter, float left_space, float right_space, float top_space, float bottom_space, uint32_t attrs);

#endif

