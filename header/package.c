#include "package.h"
#include <stdio.h>
#include <math.h>
#include "attr.h"

static void package_frame(float left, float top, float right, float bottom, int layer);

void package(char *name, int start, int end, int rows, float pitch, float row_pitch, float drill, float diameter, float left_space, float right_space, float top_space, float bottom_space, uint32_t attrs)
{
	float y_loc, x_loc, radius;
	float top, bottom, left, right;
	int i, row, column, pin;

	puts("\t\t\t<packages>");

	for (i = start; i <= end; i++) {
		printf("\t\t\t\t<package name=\"%s-%02d\">\n", name, i);

		y_loc = 0.0 - ((row_pitch * (rows - 1)) / 2);
		if (attrs & ATTR_FLIP_ROWS)
			y_loc = -y_loc;

		for (row = 0; row < rows; row++) {
			x_loc = 0.0 - ((pitch * (i - 1)) / 2);
			for (column = 0; column < i; column++) {
				if (attrs & ATTR_FLIP_PIN_NAMES)
					pin = (row * i) + column + 1;
				else
					pin = ((column * rows) + row) + 1;

				if ((column == 0) && (row == 0) && (attrs & ATTR_FIRST_SQUARE)) {
					if (diameter)
						printf("\t\t\t\t\t<pad name=\"%d\" x=\"%.02f\" y=\"%.02f\" drill=\"%.02f\" diameter=\"%.02f\" shape=\"square\"/>\n", pin, x_loc, y_loc, drill, diameter);
					else
						printf("\t\t\t\t\t<pad name=\"%d\" x=\"%.02f\" y=\"%.02f\" drill=\"%.02f\" shape=\"square\"/>\n", pin, x_loc, y_loc, drill);
				} else {
					if (diameter)
						printf("\t\t\t\t\t<pad name=\"%d\" x=\"%.02f\" y=\"%.02f\" drill=\"%.02f\" diameter=\"%.02f\"/>\n", pin, x_loc, y_loc, drill, diameter);
					else
						printf("\t\t\t\t\t<pad name=\"%d\" x=\"%.02f\" y=\"%.02f\" drill=\"%.02f\"/>\n", pin, x_loc, y_loc, drill);
				}
				x_loc += pitch;
			}
			if (attrs & ATTR_FLIP_ROWS)
				y_loc -= row_pitch;
			else
				y_loc += row_pitch;
		}

		y_loc = 0.0 - ((row_pitch * (rows - 1)) / 2);
		if (attrs & ATTR_FLIP_ROWS)
			y_loc = -y_loc;

		x_loc = 0.0 - ((pitch * (i - 1)) / 2);

		if (diameter)
			radius = (diameter / 2);
		else
			radius = (pitch / 4);

		left = 0.0 - ((pitch * (i - 1)) / 2) - left_space;
		top = ((row_pitch * (rows - 1)) / 2) + top_space;
		right = ((pitch * (i - 1)) / 2) + right_space;
		bottom = 0.0 - ((row_pitch * (rows - 1)) / 2) - bottom_space;

		package_frame(left, top, right, bottom, 21);
		package_frame(left, top, right, bottom, 51);
		if (attrs & ATTR_FIRST_SQUARE)
			package_frame(x_loc - radius, y_loc - radius, x_loc + radius, y_loc + radius, 51);
		else
			printf("\t\t\t\t\t<circle x=\"%.02f\" y=\"%.02f\" radius=\"%.02f\" width=\"0.2\" layer=\"51\"/>\n", x_loc, y_loc, radius);

		if (attrs & ATTR_BOTTOM_PLACE)
			package_frame(left, top, right, bottom, 22);

		if (attrs & ATTR_BOTTOM_DOC) {
			package_frame(left, top, right, bottom, 52);
			if (attrs & ATTR_FIRST_SQUARE)
				package_frame(x_loc - radius, y_loc - radius, x_loc + radius, y_loc + radius, 52);
			else
				printf("\t\t\t\t\t<circle x=\"%.02f\" y=\"%.02f\" radius=\"%.02f\" width=\"0.2\" layer=\"51\"/>\n", x_loc, y_loc, radius);
		}

		printf("\t\t\t\t\t<text x=\"-1\" y=\"%.02f\" size=\"0.8128\" layer=\"25\" font=\"vector\">&gt;Name</text>\n", roundf((top + 0.5) * 10) / 10);
		printf("\t\t\t\t\t<text x=\"-1\" y=\"-0.5\" size=\"0.8128\" layer=\"51\" font=\"vector\">&gt;Name</text>\n");

		if (attrs & ATTR_BOTTOM_NAME)
			printf("\t\t\t\t\t<text x=\"1\" y=\"%.02f\" size=\"0.8128\" layer=\"26\" font=\"vector\" rot=\"MR0\">&gt;Name</text>\n", roundf((top + 0.5) * 10) / 10);

		if (attrs & ATTR_BOTTOM_NAME_DOC)
			printf("\t\t\t\t\t<text x=\"1\" y=\"-0.5\" size=\"0.8128\" layer=\"52\" font=\"vector\" rot=\"MR0\">&gt;Name</text>\n");

		puts("\t\t\t\t</package>");
	}

	puts("\t\t\t</packages>");
}

static void package_frame(float left, float top, float right, float bottom, int layer)
{
	printf("\t\t\t\t\t<wire x1=\"%.02f\" y1=\"%.02f\" x2=\"%.02f\" y2=\"%.02f\" width=\"0.2\" layer=\"%d\"/>\n", left, top, right, top, layer);
	printf("\t\t\t\t\t<wire x1=\"%.02f\" y1=\"%.02f\" x2=\"%.02f\" y2=\"%.02f\" width=\"0.2\" layer=\"%d\"/>\n", right, top, right, bottom, layer);
	printf("\t\t\t\t\t<wire x1=\"%.02f\" y1=\"%.02f\" x2=\"%.02f\" y2=\"%.02f\" width=\"0.2\" layer=\"%d\"/>\n", right, bottom, left, bottom, layer);
	printf("\t\t\t\t\t<wire x1=\"%.02f\" y1=\"%.02f\" x2=\"%.02f\" y2=\"%.02f\" width=\"0.2\" layer=\"%d\"/>\n", left, bottom, left, top, layer);
}

