#!/usr/bin/python3
# coding: utf-8

import os
import sys
import time
import signal
import getopt
import tempfile
from lxml import etree

tempdir = None

def signal_handler(sig, frame):
	print
	if tempdir != None:
		os.system('rm -rf "' + tempdir + '"')
	sys.exit(255)

signal.signal(signal.SIGINT, signal_handler)

frames = {}
sheets = []

eagle_command = '/opt/eagle-6.1.0/bin/eagle'
eagle_file = None
output_name = None

# opts
opts, args = getopt.getopt(sys.argv[1:], 's:o:')

# parse opts & read conversion files
for opt in opts:
	if opt[0] == '-s':
		eagle_file = opt[1]
	elif opt[0] == '-o':
		output_name = opt[1]

if len(args) > 0:
	eagle_file = args[0]

input_name = os.path.splitext(eagle_file)[0]
basename = os.path.basename(eagle_file)
if output_name == None:
	output_name = os.path.splitext(basename)[0] + '_SCH.pdf'

# load eagle file
eagle = etree.parse(eagle_file)

# parse frames
eagle_parts = eagle.xpath('/eagle/drawing/schematic/parts')[0]
for eagle_part in eagle_parts:
	if 'name' in eagle_part.attrib and 'library' in eagle_part.attrib and 'deviceset' in eagle_part.attrib and eagle_part.attrib['library'] == 'frames':
		if eagle_part.attrib['deviceset'] == 'A5L-LOC' or eagle_part.attrib['deviceset'] == 'A4L-LOC' or eagle_part.attrib['deviceset'] == 'A3L-LOC':
			frames[eagle_part.attrib['name']] = eagle_part.attrib['deviceset']

# parse sheets
num_of_sheets = 0
eagle_sheets = eagle.xpath('/eagle/drawing/schematic/sheets')[0]
for eagle_sheet in eagle_sheets:
	sheet_ok = False
	num_of_sheets = num_of_sheets + 1
	for eagle_instances in eagle_sheet:
		if eagle_instances.tag != 'instances':
			continue
		for eagle_instance in eagle_instances:
			if 'part' in eagle_instance.attrib and eagle_instance.attrib['part'] in frames:
				sheets.append(frames[eagle_instance.attrib['part']])
				sheet_ok = True
				break
		if sheet_ok:
			break
	if not sheet_ok:
		print('sheet ' + str(sheet) + 'is missing a frame')
		sys.exit(1)

convert_command = 'pdfunite'
tempdir = tempfile.mkdtemp(prefix='schematic_', dir='/tmp')
sheet = 0
for frame in sheets:
	sheet = sheet + 1
	source_file = input_name
	if sheet == 1:
		source_file = source_file + '.sch'
	else:
		source_file = source_file + '.s' + str(sheet)
	if frame == 'A5L-LOC':
		eagle_device = 'PS'
		eagle_scale = 1.3
		eagle_rotation = '-r'
		eagle_offset_x = 0.3
		eagle_offset_y = 0.5
		ps_orientation = '3'
	elif frame == 'A4L-LOC':
		eagle_device = 'PS'
		eagle_scale = 1
		eagle_rotation = ''
		eagle_offset_x = 0.5
		eagle_offset_y = 0.2
		ps_orientation = '1'
	elif frame == 'A3L-LOC':
		eagle_device = 'PS_DINA3'
		eagle_scale = 1
		eagle_rotation = ''
		eagle_offset_x = 0.5
		eagle_offset_y = 0.5
		ps_orientation = '1'
	sch_command = eagle_command + ' -X -d' + eagle_device + ' -o"' + tempdir + '/sch' + str(sheet) + '.ps" -x' + str(eagle_offset_x) + ' -y' + str(eagle_offset_y) + ' ' + eagle_rotation + ' -s' + str(eagle_scale) + ' "' + source_file + '" 91 92 94 95 96 97 98 2>&1 | egrep -v "contains a polygon|Continue?|Yes No|EAGLE Version"'
	ps_command = 'gs -sDEVICE=pdfwrite -sOutputFile="' + tempdir + '/sch' + str(sheet) + '.pdf" -dNOPAUSE -dEPSCrop -dAutoRotatePages=/None -c "<</Orientation ' + ps_orientation + '>> setpagedevice" -f "' + tempdir + '/sch' + str(sheet) + '.ps" -c quit'
	convert_command = convert_command + ' "' + tempdir + '/sch' + str(sheet) + '.pdf"'
	print('\033[1;36mGenerating sheet' + str(sheet) + ':\033[0m')
	c = os.popen(sch_command, 'w', 1)
	while True:
		try:
			c.write('Yes\n')
			time.sleep(0.001)
		except:
			break
	# c.close()
#	os.system('cp "' + tempdir + '/sch' + str(sheet) + '.ps" .')

	if os.system(ps_command) > 0:
		signal_handler(signal.SIGINT, 0)

convert_command = convert_command + ' ' + output_name
os.system(convert_command)

os.system('rm -rf "' + tempdir + '"')

